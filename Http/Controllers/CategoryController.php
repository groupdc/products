<?php

namespace Dcms\Products\Http\Controllers;

use App\Http\Controllers\Controller;

use Dcms\Products\Models\Category;

use Illuminate\Http\Request;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;
use Config;

class CategoryController extends Controller
{

    /**
     * Set permissions.
     *
     */
    public function __construct()
    {
        $this->middleware('permission:products-browse')->only('index');
        $this->middleware('permission:products-add')->only(['create', 'store']);
        $this->middleware('permission:products-edit')->only(['edit', 'update']);
        $this->middleware('permission:products-delete')->only('destroy');
    }

    public static function rebuildTree()
    {
        Category::rebuild();

        return "rebuild ok";
    }

    public static function QueryTree()
    {
        $tree = DB::connection('project')
            ->table('products_categories_language as node')
            ->select(
                (DB::connection("project")->raw("CONCAT( REPEAT( '-', node.depth ), node.title) AS category")),
                "node.id",
                "node.parent_id",
                "node.language_id",
                "node.depth",
                (DB::connection("project")->raw('Concat("<img src=\'/packages/dcms/core/images/flag-",lcase(country),".png\' >") as regio'))
            )
            ->leftJoin('languages', 'node.language_id', '=', 'languages.id')
            ->orderBy('node.lft')
            ->get();

        return $tree;
    }

    public static function CategoryDropdown($models = null, $selected_id = null, $enableNull = true, $name = "parent_id", $key = "id", $value = "category")
    {
        $dropdown = "empty set";
        if (!is_null($models) && count($models) > 0) {
            $dropdown = '<select name="' . $name . '" class="form-control" id="parent_id">' . "\r\n";

            if ($enableNull == true) {
                $dropdown .= '<option value="">None</option>'; //epty value will result in NULL database value;
            }

            foreach ($models as $model) {
                $selected = "";
                if (!is_null($selected_id) && $selected_id == $model->$key) {
                    $selected = "selected";
                }

                //altering these tag properties can affect the form (jQuery)
                $dropdown .= '<option ' . $selected . ' value="' . $model->$key . '" class="' . $name . ' language_id' . $model->language_id . ' parent-' . (is_null($model->parent_id) ? 0 : $model->parent_id) . ' depth-' . $model->depth . '">' . $model->$value . '</option>' . "\r\n";
            }
            $dropdown .= '</select>' . "\r\n" . "\r\n";
        }

        return $dropdown;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //Category::rebuild();
        // load the view and pass the categories
        return View::make('dcmsproducts::categories/index');
    }


    public function getDatatable()
    {
        return DataTables::queryBuilder(DB::connection('project')
            ->table('products_categories_language as node')
            ->select(
                (DB::connection("project")->raw("CONCAT( REPEAT( '-', node.depth ), node.title) AS category")),
                "node.id",
                (DB::connection("project")->raw('Concat("<img src=\'/packages/dcms/core/images/flag-",lcase(country),".png\' >") as country'))
            )
            ->leftJoin('languages', 'node.language_id', '=', 'languages.id')
            ->orderBy('node.lft')
        )
            ->addColumn('edit', function ($model) {
                $edit = '<form method="POST" action="/admin/products/categories/' . $model->id . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE">';
                if (Auth::user()->can('products-edit')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/products/categories/' . $model->id . '/edit"><i class="fa fa-pencil"></i></a>';
                }
                if(Auth::user()->can('products-delete')){
                    $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this product category" onclick="if(!confirm(\'Are you sure to delete this item ? \')){return false;};"><i class="fa fa-trash-o"></i></button>';
                }

                $edit .= '</form>';

                return $edit;
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $oLanguages = DB::connection("project")
            ->table("languages")
            ->select((DB::connection("project")
                ->raw(" '' as title,'' as description_short, '' as description,'' image ")), "id", "id as thelanguage_id", "language", "country", "language_name")->get();

        // load the create form (app/views/categories/create.blade.php)
        return View::make('dcmsproducts::categories/form')
            ->with('oLanguages', $oLanguages)
            ->with('categoryOptionValues', $this->CategoryDropdown($this->QueryTree()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = ['title' => 'required', 'language_id' => 'required'];
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/products/categories/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $theParent = null;
            $prefixPath = "";
            //find root ellement
            if ($request->has("parent_id") && intval($request->get("parent_id")) > 0) {
                $theParent = Category::find($request->get("parent_id"));
            }

            if (is_null($theParent)) {
                //$theParent = Category::where('language_id','=',$request->get("language_id"))->where('depth','=','0')->first();
            } else {
                $prefixPath = $theParent->url_path . '/';
            }

            $node = new Category;
            $node->language_id = $request->get('language_id');
            $node->title = $request->get('title');
            $node->description_short = $request->get('description_short');
            $node->description = $request->get('description');
            $node->image = $request->get('image');
            $node->url_slug = str_slug($request->get('title'));
            $node->url_path = $prefixPath . str_slug($request->get('title'));
            $node->save();

            if (is_null($theParent)) {
                $node->makeRoot();
            } else {
                $node->makeChildOf($theParent);
            }

            if ($request->has('nexttosiblingid') && intval($request->get('nexttosiblingid')) > 0) {
                $node->moveToLeftOf(intval($request->get('nexttosiblingid')));
            }
        }

        Session::flash('message', 'Successfully created category!');

        return Redirect::to('admin/products/categories');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //	get the category
        $category = Category::find($id);

        $oLanguages = DB::connection("project")->select('SELECT languages.id as thelanguage_id, language, country, language_name, products_categories_language.*
			FROM  products_categories_language
			LEFT JOIN languages on products_categories_language.language_id = languages.id
			WHERE  languages.id is not null AND  products_categories_language.id= ?
			UNION
			SELECT languages.id , language, country, language_name, \'\' , \'\' , languages.id , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'\', \'\' , \'\', \'\', \'\'
			FROM languages
			WHERE id NOT IN (SELECT language_id FROM products_categories_language WHERE id = ?) ORDER BY 1
			', [$id, $id]);

        return View::make('dcmsproducts::categories/form')
            ->with('category', $category)
            ->with('oLanguages', $oLanguages)
            ->with('categoryOptionValues', $this->CategoryDropdown($this->QueryTree(), $category->parent_id));
    }

    /**
     * copy the model
     *
     * @param  int $id
     *
     * @return Response
     */
    public function copy($id)
    {
        //NOT SUPPORTED
        return Redirect::to('admin/products/categories');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        // validate
        $rules = ['title' => 'required', 'language_id' => 'required'];
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/products/categories/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            $theParent = null;
            $prefixPath = "";
            //find root ellement
            if ($request->has("parent_id") && intval($request->get("parent_id")) > 0) {
                $theParent = Category::find($request->get("parent_id"));
            }

            if (is_null($theParent)) {
                //$theParent = Category::where('language_id','=',$request->get("language_id"))->where('depth','=','0')->first();
            } else {
                $prefixPath = $theParent->url_path . '/';
            }

            $node = Category::find($id);
            $node->language_id = $request->get('language_id');
            $node->title = $request->get('title');
            $node->description_short = $request->get('description_short');
            $node->description = $request->get('description');
            $node->image = $request->get('image');
            $node->url_slug = str_slug($request->get('title'));
            $node->url_path = $prefixPath . str_slug($request->get('title'));
            $node->save();

            $setRoot = false;
            $theParent = null;
            $moveParent = true;

            if (intval($request->get('parent_id')) > 0 && $request->get('parent_id') <> $node->parent_id) {
                //move to a new parentid
                $moveParent = true;
                $theParent = Category::find($request->get("parent_id"));
            } elseif (intval($request->get('parent_id')) <= 0 && $request->get('parent_id') <> $node->parent_id) {
                //move to a ROOT of the same, or other language
                $moveParent = true;
                //$theParent = Category::where('language_id','=',$request->get("language_id"))->where('depth','=','0')->first();
                if (is_null($theParent)) {
                    $setRoot = true;
                }
            } else {
                //we stay in the same parent
                $moveParent = false;
            }
            if ($setRoot == true) {
                $node->makeRoot();
            } elseif (!is_null($theParent)) {
                $node->makeChildOf($theParent);
            }

            if ($moveParent == false && $request->has('nexttosiblingid') && intval($request->get('nexttosiblingid')) > 0 && $request->get("oldsort") < $request->get("sort")) {
                $node->moveToRightOf(intval($request->get('nexttosiblingid')));
            } elseif ($moveParent == false && $request->has('nexttosiblingid') && intval($request->get('nexttosiblingid')) > 0 && $request->get("oldsort") > $request->get("sort")) {
                $node->moveToLeftOf(intval($request->get('nexttosiblingid')));
            } elseif ($moveParent == false && $request->get("oldsort") < $request->get("sort")) {
                $node->makeLastChildOf($node->parent_id);
            } elseif ($moveParent == true && $request->has('nexttosiblingid') && intval($request->get('nexttosiblingid')) > 0) {
                $node->moveToRightOf(intval($request->get('nexttosiblingid')));
            }

            // redirect
            Session::flash('message', 'Successfully updated category!');

            return Redirect::to('admin/products/categories');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $category = Category::find($id);
        $category->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the category!');

        return Redirect::to('admin/products/categories');
    }
}

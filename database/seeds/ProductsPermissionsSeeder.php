<?php

namespace Dcms\Products\Database\Seeds;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ProductsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = ['products'];
        $permission_actions = ['browse', 'read', 'edit', 'add', 'delete'];

        foreach ($modules as $module) {
            Permission::firstOrCreate(['name' => $module, 'module' => $module, 'action' => '', 'level' => 0]);

            foreach ($permission_actions as $permission_action) {
                Permission::firstOrCreate(['name' => $module . '-' . $permission_action, 'module' => $module, 'action' => $permission_action, 'level' => 1]);
            }
        }
    }
}

<?php

return [
    "Products" => [
        "icon"  => "fa-shopping-cart",
        "links" => [
            ["route" => "admin/products", "label" => "Products", "permission" => 'products'],
            ["route" => "admin/products/categories", "label" => "Categories", "permission" => 'products'],
        ],
    ],
];
?>
